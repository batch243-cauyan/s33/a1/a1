/*

---- JSON Placeholder free JSON API --------------------------

https://jsonplaceholder.typicode.com/posts 		| 100 posts
https://jsonplaceholder.typicode.com/comments	| 500 comments
https://jsonplaceholder.typicode.com/albums	    | 100 albums
https://jsonplaceholder.typicode.com/photos	    | 5000 photos
https://jsonplaceholder.typicode.com/todos		| 200 todos
https://jsonplaceholder.typicode.com/users		| 10 users

Fetch single entity (add /:id):

https://jsonplaceholder.typicode.com/posts/1
https://jsonplaceholder.typicode.com/users/3
https://jsonplaceholder.typicode.com/photos/12


*/


// console.log(fetch('https://jsonplaceholder.typicode.com/todos')
// );


//3-4
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=>{return response.json();})
.then((json)=>{
 const todos = json.map((todo)=>{
        return todo.title;
 });
 console.log(todos);
})



//5-6
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`This is item title:"${json.title}" and its status:"${json.completed}"`));






async function createThenUpdates(){

    //7
    // Creates a new post following the Rest API (create, /posts/:id, POST)
  await fetch('https://jsonplaceholder.typicode.com/todos', {

        method: 'POST',

        headers: {
            'Content-type': 'application/json',
        },
        
        body: JSON.stringify({
        completed: true,
        id: 201,
        title: "Created To Do List Item",
        userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json));


    // 8
 await  fetch('https://jsonplaceholder.typicode.com/todos/200', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	id: 202,
	  	title: 'I UPDATED THIS POST FOR S33',
	  	body: 'S33 I FINISHED THIS',
	  	userId: 2
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json));

    // 9
    await  fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({
              dateCompleted: 'Nov. 10, 1986',
              description: 'S33 I PATCHED THIS ITEM',
              id:1,
              Status: 'Pending',
              Title: 'Updated To Do List Item',
              userId: 1
            })
        })
        .then((response) => response.json())
        .then((json) => console.log(json));

    //10-11
    await  fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PATCH',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({
            completed: false,
            Date_Completed: '07/09/21', 
            Date_Changed: 'November 29, 2022',
            id:1,
            status: 'Complete',
            title: 'delectus aut auten',
            userId: 1
            })
        })
        .then((response) => response.json())
        .then((json) => console.log(json));



    };

  

createThenUpdates();


//12
fetch('https://jsonplaceholder.typicode.com/posts/199', {
  method: 'DELETE'
})


